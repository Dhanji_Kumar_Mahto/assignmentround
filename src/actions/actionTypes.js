export const addItemToCart = (item) =>{
    return {
        type: "ADD_ITEM_TO_CART",
        deviceName: item.deviceName,
    }
}

export const addQuantityToCart = (item) =>{
    return {
        type: "ADD_QUANTITY_TO_CART",
        deviceName: item.deviceName,
    }
}

export const removeQuantityFromItem = (item) =>{
    return {
        type: "REMOVE_QUANTITY_FROM_ITEM",
        deviceName: item.deviceName,
    }
}

export const removeItemFromCart = (item) =>{
    return {
        type: "REMOVE_ITEM_FROM_CART",
        deviceName: item.deviceName,
    }
}