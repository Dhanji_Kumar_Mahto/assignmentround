import React, { Component } from 'react';
import {connect} from 'react-redux';
import LoginView from './template/mobileView/LoginView'
import history from '../history';
import Routes from '../Routes'
class MainComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userLanded: true,
            isUserDetailPage:false
        };

        this.loginClicked = this.loginClicked.bind(this);
    }
   
    loginClicked = (isValidationTrue) => {
        if( isValidationTrue) {
            history.push('/userDetails');
            this.setState({userLanded:false});
        }
    }

    render() { 
        return ( 
            <>
                <div className="container">
                        {this.state.userLanded && <LoginView loginClicked = {this.loginClicked}/>}
                </div>  
            </>
        );
    }
}

function mapStateToProps (state) {
    return{

    };
}

function mapDispatchToProps (dispatch) {
    return {

    };
}


export default connect(mapStateToProps,mapDispatchToProps)(MainComponent); 
