import {useState} from 'react';
import {useHistory,
    BrowserRouter as Router,
} from "react-router-dom";

function LoginView(props) {

    const [errors,setState] = useState({userError:null,passwordError:null,phoneNumberError:null});
    let history = useHistory();
    const validateData = () => {
        let validationStatus = true;
       /* setState({userError:null,passwordError:null,phoneNumberError:null});
        if(document.getElementById("userName").value.length === 0) {
            setState((prevState) => ({...prevState,userError:"Please provide userName"}));
            validationStatus = false;
        }

        if(document.getElementById("password").value.length === 0) {
            setState((prevState) => ({...prevState,passwordError:"Please provide password"}));
            validationStatus = false;
        }

        if(document.getElementById("phoneNumber").value.length === 0 || 
        document.getElementById("phoneNumber").value.length <10 ||
        document.getElementById("phoneNumber").value.length > 10
        || isNaN(document.getElementById("phoneNumber").value)
        ) {
            setState((prevState) => ({...prevState,phoneNumberError:" Please input phone number correctly"}));
            validationStatus = false;
        } */
        props.loginClicked(validationStatus);
    }

    return (
        <Router>
            <div className="loginBox">
                <div className="smallcontainer">
                    <label>User name <label className="errorClass">*</label></label>
                    <input type="text" id="userName"></input>
                    <div>
                        {errors.userError && <label className="errorClass">{errors.userError}</label>}
                    </div>
                </div>

                <div className="smallcontainer">
                <label>Password <label className="errorClass">*</label></label>
                <input type="password" id="password"></input>
                    <div>
                        {errors.passwordError && <label className="errorClass">{errors.passwordError}</label>}
                    </div>
                </div>

                <div className="smallcontainer">    
                <label> Id</label>
                <input type="text"></input>
                </div>

                <div className="smallcontainer">
                <label> Name</label>
                <input type="text"></input>
                </div>

                <div className="smallcontainer">
                <label> Address</label>
                <input type="textarea"></input>
                </div>

                <div className="smallcontainer">
                    <label> Phone <label className="errorClass">*</label></label>
                    <input type="text" id="phoneNumber"></input>
                    <div>
                        {errors.phoneNumberError && <label className="errorClass">{errors.phoneNumberError}</label>}
                    </div>
                </div>
                <div className = 'smallcontainer role'>
                    <div>
                        <label>Role</label>
                    </div>
                    <div>
                        <input type="radio" id="admin" name="role"></input>
                        <label>Admin</label>
                        <input type="radio" id="normal" name="role"></input>
                        <label>normal</label>
                    </div>

                </div>
                <div className="button">
                    <button onClick = {() => validateData()}> Login</button>
                </div>
            </div>
        </Router>
    );

}
  
  export default LoginView;
  