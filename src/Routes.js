import {Router,Switch,Route} from "react-router-dom";
import history from './history';
import UserDetails from '../src/components/template/mobileView/userDetails';
import {Component} from 'react';
class Routes extends Component {
    
    render() { 
        return (  
            <Router history={history}>
                <Switch>
                    <Route exact path='/'></Route>
                    <Route path='/userDetails' component={UserDetails}></Route>
                </Switch>
            </Router>

        );
    
}}
    
export default Routes;