import { createReducer } from '@reduxjs/toolkit'
let initialState = {
        deviceList:[]
}

export const deviceReducer = createReducer(initialState, {
    DEVICE_LOADED: (state, action) => {
        state.deviceList = action.deviceList;
    }
  })