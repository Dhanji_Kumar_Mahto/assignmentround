import {deviceLoaded} from '../../actions/deviceActionTypes';


export const getDevices = (dispatch) => {
     fetch('http://localhost:3000/devices').then((resp) => {
            resp.json().then((result) => {
                dispatch(deviceLoaded(result))
            })
        });
}